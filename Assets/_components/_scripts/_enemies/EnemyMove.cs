﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {
	public GameObject Enemy;
	public float MoveSpeed;
	public int XmoveDirection;
	private Rigidbody2D rb; 

	[SerializeField]
	private string Enemytype = "walker";
	// enemy types whould be walker, fly
	[SerializeField]
	private float distance = 1;

	[SerializeField]
	private int flyPower = 2;
	[SerializeField]
	private int distanceTofly = 2;
	[SerializeField]
	private int loop = 0;

	[SerializeField]
	private int repultionPowerX = 1000;
	[SerializeField]
	private int repultionPowerY = 700;

	// Use this for initialization
	void Awake() {
		if (Enemy == null) {
			Enemy = gameObject;
		}
	}

	void Start () {
		rb = Enemy.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		EnemyBehavior (Enemytype);
	}

	public bool HitPlayer(GameObject player) {
		PlayerHealth playerHealth = player.GetComponent<PlayerHealth> ();
		Rigidbody2D erb = player.GetComponent<Rigidbody2D> ();

		if (playerHealth.health > 0) {
			playerHealth.health += -2;

			rb.velocity = Vector2.zero;
			if (XmoveDirection > 0) {
				erb.AddForce (new Vector2 (repultionPowerX * -1, repultionPowerY), ForceMode2D.Impulse);
			} else {
				erb.AddForce (new Vector2 (repultionPowerX * -1, repultionPowerY), ForceMode2D.Impulse);
			}

			player.GetComponent<Animator>().SetFloat("speed", 0);
			player.GetComponent<PlayerMove> ().isGrounded = false;
			return true;
		} else {
			return false;
		}
	}

	void EnemyBehavior(string type) {
		switch(type){
			case "walker":
				walkerMove ();
			break;
		case "fly":
			flyMove ();
			break;
		}
	}

	void walkerMove() {
		RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(XmoveDirection, 0));
		rb.velocity = new Vector2(XmoveDirection, 0) * MoveSpeed;

		if (hit != null && hit.collider != null) {
			if (hit.distance < distance) {
				Flip ();
			}
		}
	}

	void flyMove() {
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);

		if (hit != null && hit.collider != null) {
			if (hit.distance <= distanceTofly && hit.collider.tag != "enemy") {
				rb.velocity = new Vector2(XmoveDirection * MoveSpeed , 1 * flyPower);
			}
		} else {
			rb.velocity = Vector2.right * 3;
		}

		if (loop <= 1) {
			Flip ();
			loop = 20;
		}

		loop -= 1;
	}

	void Flip() {
		if (XmoveDirection > 0) {
			XmoveDirection = -1;
		} else {
			XmoveDirection = 1;
		}
			
		Vector2 localScale = Enemy.transform.localScale;
		localScale.x *= -1;
		transform.localScale = localScale;
	}
		
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "player") {
			HitPlayer (coll.gameObject);
		} else {
			Flip ();
		}
	}
}
