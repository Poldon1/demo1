﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class DataManagement : MonoBehaviour {
	public static DataManagement datamanagement;
	public int highscore;

	void Awake() {
		if (datamanagement == null) {
			DontDestroyOnLoad (gameObject);
			datamanagement = this;
		} else if (datamanagement != this) {
			Destroy (gameObject);
		}
	}

	public void SaveData () {
		BinaryFormatter binForm = new BinaryFormatter (); // bin formatter
		FileStream file = File.Create (Application.persistentDataPath + "/gameInfo.dat"); //create save file
		gameData data = new gameData(); // create container for data
		data.highscore = highscore;
		binForm.Serialize (file, data);
		file.Close ();
	}

	public void LoadData () {
		if (File.Exists (Application.persistentDataPath + "/gameInfo.dat")) {
			BinaryFormatter binForm = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/gameInfo.dat", FileMode.Open);
			gameData data = (gameData)binForm.Deserialize (file);
			file.Close ();
			highscore = data.highscore;
		}
	}
}

[Serializable]
class gameData {
	public int highscore;
}
