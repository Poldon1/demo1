﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scores : MonoBehaviour {
	public string Scene;
	public int totalScore = 0;
	[SerializeField]
	private GameObject scoreObject;

	void Start() {
		DataManagement.datamanagement.LoadData ();
		Debug.Log ("start score");
		Debug.Log (DataManagement.datamanagement.highscore);
	}
	// Update is called once per frame
	void Update () {
		scoreObject.gameObject.GetComponent<Text>().text = "Score: " + totalScore;
	}

	void OnTriggerEnter2D(Collider2D cl) {
		if (cl.gameObject.tag == "end") {
			SceneManager.LoadScene(Scene);
			DataManagement.datamanagement.SaveData ();
			CounterScore ();
		}

		bool cCoin = true;

		if (cl.gameObject.tag == "coin" && cCoin) {
			cCoin = false;
			totalScore += 10;
			Destroy(cl.gameObject);
		}
	}

	void CounterScore() {
		DataManagement.datamanagement.highscore = totalScore;
		DataManagement.datamanagement.SaveData ();	
	}
}
