﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAttackTrigger : MonoBehaviour {
	public int dmg = 20;

	void OnTriggerEnter2D(Collider2D col) {
		if(!col.isTrigger && col.CompareTag("enemy")) {
			col.gameObject.GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 200f);
			col.SendMessageUpwards ("Damage", dmg);
		}
	}

}
