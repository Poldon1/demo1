﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
	public int health;
	public string Scene = "main";
	private GameObject player;
	[SerializeField]
	private GameObject healthObject;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("player");
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.y < -7) {
			Die ();
		}

		healthObject.gameObject.GetComponent<Text>().text = "" + health;

		if (health < 1) {
			Die ();
		}
	}

	void Die () {
		SceneManager.LoadScene(Scene);
	}
}
