﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public GameObject player;
	[SerializeField]
	private int playerSpeed = 10;
	[SerializeField]
	public bool facingRight = false;
	[SerializeField]
	private float moveX;
	[SerializeField]
	public bool isGrounded;

	[SerializeField]
	private Transform[] groundPoints;
	[SerializeField]
	private int playerJumpPower = 1250;
	private int JumpNumber = 0;
	[SerializeField]
	private Transform[] rigthPoints;

	[SerializeField]
	private LayerMask whatIsGround;

	public bool isAttack = false;

	private Animator animator;
	private Rigidbody2D rb;
	private bool isHit = false;

	[SerializeField]
	private int jumpRepultionForce = 30000;
	[SerializeField]
	private int secondJumpPower = 15000;

	void Awake () {
		animator =  player.GetComponent<Animator>();
		rb = player.GetComponent<Rigidbody2D> (); 
	}
	// Update is called once per frame
	void Update () {
		//isGrounded = Grounded();
		PlayerMovement();
		HandleAttacks();
		playerRayCast();
	}

	void PlayerMovement() {
		//controls
		moveX = Input.GetAxis("Horizontal");
		if (Input.GetButtonDown("Jump") && (isGrounded || JumpNumber < 2) && !isHit) {
			Jump();
		}
		//animations
		//player direction
		if (moveX < 0.0f && !facingRight) {
			FlipPlayer ();	
		} else if (moveX > 0.0f && facingRight) {
			FlipPlayer ();
		}
		//physics
		if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("isAttack") && !isHit) {
			rb.velocity = new Vector2 (moveX * playerSpeed, rb.velocity.y);
		}

		if (isGrounded) {
			animator.SetFloat ("speed", Mathf.Abs (moveX));
		} else {
			animator.SetFloat("speed", 0);
		}
	}

	void Jump() {
		if (JumpNumber < 2) {
			animator.SetFloat ("Jump", JumpNumber + 1);


			if (JumpNumber > 0) {
				rb.AddForce ((Vector2.up * secondJumpPower));
			} else {
				rb.AddForce (Vector2.up * playerJumpPower);
			}

			JumpNumber++;
			isGrounded = false;
		}
	}

	void HandleAttacks () {
		PlayerAttack playerAttack = player.GetComponent<PlayerAttack> ();

		if (Input.GetButtonDown("Fire3") && !isAttack && JumpNumber == 0){
			isAttack = true;
			playerAttack.TriggerAttack (true);
		}
	} 

	void FlipPlayer() {
		facingRight = !facingRight;
		Vector2 localScale = player.transform.localScale;
		localScale.x *= -1;
		transform.localScale = localScale;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (Grounded()) {
			isGrounded = true;
			isHit = false;
			JumpNumber = 0;
			animator.SetFloat("Jump", JumpNumber);
		}
	}

	private bool Grounded() {
		if(rb.velocity.y <= 0) {
			foreach(Transform point in groundPoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, 0.2f, whatIsGround);

				for(int i = 0; i < colliders.Length; i++) {
					if (colliders[i].gameObject != player) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private bool canGrab() {
		if(JumpNumber > 0 && rb.velocity.x != 0) {
			foreach (Transform point in rigthPoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, 0.1f , whatIsGround);

				for(int i = 0; i < colliders.Length; i++) {
					if (colliders[i].gameObject != player) {
						Debug.Log(colliders[i].gameObject.tag);
						return true;
					}
				}
			}
		}

		return false;
	}

	private void playerRayCast() {
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
		if(hit != null && hit.collider != null) {
			if (hit.distance < 2f && hit.collider.tag == "enemy" && hit.collider.tag != "gPoint") {
				rb.AddForce(Vector2.up * jumpRepultionForce);
				isGrounded = false;
				JumpNumber = 2;
				animator.SetFloat("Jump", 2);
			}
		}
	}
}
