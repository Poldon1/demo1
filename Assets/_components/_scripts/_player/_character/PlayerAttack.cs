﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {
	private bool attacking = false;

	private float attackTimer = 0;

	[SerializeField]
	private float attackCd = 0.5f;

	public Collider2D attackTrigger;

	private Animator anim;

	// Use this for initialization
	void Awake () {
		anim = gameObject.GetComponent<Animator> ();
		attackTrigger.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (attacking) {
			if (attackTimer > 0) {
				attackTimer -= Time.deltaTime;
			} else {
				attacking = false;
				attackTrigger.enabled = false;
				gameObject.GetComponent<PlayerMove> ().isAttack = false;
				anim.SetBool ("isActack", attacking);
			}
		}

		anim.SetBool ("isActack", attacking);
	}

	public void TriggerAttack(bool a) {
		attacking = a;
		attackTimer = attackCd;
		attackTrigger.enabled = true;
	}
}
